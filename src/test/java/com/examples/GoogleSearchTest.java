package com.examples;

import com.core.Base;
import com.pages.GoogleSearchPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class GoogleSearchTest extends Base {

    @Test(description = "this test case is for search google ")
    public void googleTest() throws InterruptedException {
        GoogleSearchPage googleSearch = new GoogleSearchPage(driver);
        googleSearch.googleTest();

    }
}