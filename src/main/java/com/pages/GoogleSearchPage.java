package com.pages;

import com.core.Base;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoogleSearchPage extends Base {

    @FindBy(xpath = "//*[@id=\"tsf\"]/div[2]/div[1]/div[1]/div/div[2]/input")
    private WebElement search;
    @FindBy(xpath = "//div[@class='FPdoLc tfB0Bf']//input[@name='btnK']")
    private WebElement searchButton;

    public GoogleSearchPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getSearchButton() {
        return searchButton;
    }

    public String getSearchButtonText() {
        return searchButton.getText();
    }

    public void googleTest() {

        this.search.click();
        this.search.sendKeys("new search ");
        this.searchButton.click();

    }
}