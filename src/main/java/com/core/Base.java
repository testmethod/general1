package com.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

public class Base {

    public static WebDriver driver;

    @BeforeMethod(alwaysRun = true)
    public void beforeTest() {

        System.setProperty("webdriver.chrome.driver",
                "src/main/resources/chromedriver");
        driver = new ChromeDriver();
        driver.navigate().to("https://www.google.com");

        //System.out.println(" starting The Test Suite ");
    }

    @AfterMethod (alwaysRun = true)
    public void afterTest() {

        driver.quit();
        //driver.close();
        // System.out.println(" end the test suite  ");
    }
}